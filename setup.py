from setuptools import setup, find_packages

version = __import__('lemoulin_pages').__version__

packages = find_packages()

setup(
	name='django-lemoulin-pages',
	packages=packages,
	version=version,
	description='Static pages and texts for Django.',
	author='Yanik Proulx',
	author_email='yanikproulx@lemoulin.co',
	url='https://bitbucket.org/lemoulin/django-lemoulin-pages',
	download_url='https://bitbucket.org/lemoulin/django-lemoulin-pages',
	classifiers=[
		'Environment :: Web Environment',
		'Framework :: Django',
		'Intended Audience :: Developers',
		'License :: OSI Approved :: MIT License',
		'Operating System :: OS Independent',
		'Programming Language :: Python :: 3',
	],
	scripts=[],
	license='LICENSE.txt',
	long_description=open('README.rst').read(),
	install_requires=[
		"Django >= 1.7",
		"sorl-thumbnail >= 11.12.1b",
        "django-lemoulin-utils >= 0.3",
        "django-lemoulin-l10n >= 0.3",
        "django-lemoulin-gallery >= 0.3",
	],
    include_package_data = True,
)
