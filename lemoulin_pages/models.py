from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _
#from django.conf import settings

# Signals and receivers
from django.db.models.signals import pre_save
from django.dispatch import receiver

from lemoulin_l10n.models import MultilingualModel, MultilingualModelManager
from lemoulin_gallery.models import Media


def get_pages_image_path(instance, filename):
	from lemoulin_utils.files import clean_filename

	return "pages/{filename}".format(filename=clean_filename(filename))


class PageManager(MultilingualModelManager):
	def get_by_natural_key(self, language_code, slug):
		return self.get(language_code=language_code, slug=slug)


class Page(MultilingualModel):
	#language_code = models.CharField(verbose_name=_('language'), max_length=10, choices=settings.LANGUAGES, )
	title = models.CharField(verbose_name=_('title'), max_length=200, )
	slug = models.SlugField(verbose_name=_("Label"), max_length=50, unique=False, )
	header_title = models.CharField(verbose_name=_("Header main title"), max_length=1024, blank=True, null=True, )
	sub_title = models.CharField(verbose_name=_("Sub Title"), max_length=1024, blank=True, null=True, )
	image = models.ImageField(verbose_name=_("Image"), upload_to=get_pages_image_path, null=True, blank=True, )
	text = models.TextField(verbose_name=_('Content'), blank=True, )
	template_name = models.CharField(verbose_name=_('template name'), max_length=70, blank=True, null=True, help_text=_("Example: 'static/default.html'"), )

	#objects = PageManager()

	class Meta:
		verbose_name = _('Page')
		verbose_name_plural = _('Pages')
		ordering = ("title",)
		unique_together = (("language_code", "slug", ),)

	def __unicode__(self):
		return "%s" % (self.title)

	def natural_key(self):
		return (self.language_code, self.slug)


class PageMedia(Media):
	page = models.ForeignKey(to=Page, related_name="media")

	class Meta:
		ordering = ["position", "-created", ]
		verbose_name = _('Page Media')
		verbose_name_plural = _('Page Media')

	def __unicode__(self):
		return "%s: %s (%s)" % (self.page.title, self.title, self.media_type)


@receiver(pre_save, sender=PageMedia)
def set_page_media_data(sender, instance, **kwargs):
	"""Sets Page Media Data"""

	instance._set_media_data()


class PageSubSection(models.Model):
	page = models.ForeignKey(to=Page, related_name="subsections", )
	title = models.CharField(verbose_name=_("Title"), max_length=200, null=True, blank=True, )
	text = models.TextField(verbose_name=_('Content'), blank=True, )
	image = models.ImageField(verbose_name=_("Image"), upload_to=get_pages_image_path, null=True, blank=True, )
	credit = models.CharField(verbose_name=_("Image Credit"), max_length=200, null=True, blank=True, )
	order = models.PositiveSmallIntegerField(verbose_name=_("Display order"), null=True, blank=True, )

	def __unicode__(self):
		if self.title:
			return "%s: %s (%s)" % (self.page.title, self.title, self.order)
		else:
			return "%s (%s)" % (self.page.title, self.order)

	class Meta:
		ordering = ["order"]
		verbose_name = _("Page Sub Section")
		verbose_name_plural = _("Pages Sub Sections")


class StaticText(MultilingualModel):
	#language_code = models.CharField(verbose_name=_('Language'), max_length=10, choices=settings.LANGUAGES, )
	title = models.CharField(verbose_name=_('Title'), max_length=200, blank=True, null=True, )
	subtitle = models.CharField(verbose_name=_('Sub-Title'), max_length=200, blank=True, null=True, )
	slug = models.SlugField(verbose_name=_("Label"), max_length=50, unique=False, )
	text = models.TextField(verbose_name=_("Text"), blank=True, null=True, )

	def __unicode__(self):
		return "%s (%s)" % (self.slug, self.language_code)

	class Meta:
		unique_together = ("language_code", "slug")
		ordering = ["slug"]
		verbose_name = _("Static Text")
		verbose_name_plural = _("Static Texts")
		unique_together = (("language_code", "slug", ),)
