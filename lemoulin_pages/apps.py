from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

# Used in __init__.py
class LeMoulinPagesConfig(AppConfig):
	name = 'lemoulin_pages'
	verbose_name = _("Static Pages & Texts")
