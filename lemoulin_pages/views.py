from __future__ import unicode_literals

from django.views.generic import DetailView, TemplateView

from lemoulin_l10n.views import LanguageMixin
from . import models


class StaticPageView(LanguageMixin, DetailView):
	model = models.Page
	context_object_name = "page"
	template_name = "pages/default.html"

	def dispatch(self, request, *args, **kwargs):
		self.template = kwargs.get("template", None)

		return super(StaticPageView, self).dispatch(request, *args, **kwargs)

	def get_template_names(self):
		obj = self.get_object()

		if self.template:
			return self.template
		elif getattr(obj, "template_name", None):
			return getattr(obj, "template_name")
		else:
			return super(StaticPageView, self).get_template_names()


class TextPlainView(TemplateView):
	def render_to_response(self, context, **kwargs):
		return super(TextPlainView, self).render_to_response(context, content_type='text/plain', **kwargs)
