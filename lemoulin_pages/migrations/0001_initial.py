# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import lemoulin_pages.models


class Migration(migrations.Migration):

    dependencies = [
        ('lemoulin_gallery', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('language_code', models.CharField(max_length=10, choices=[('fr', 'Français')], verbose_name='Language', default='fr-ca')),
                ('title', models.CharField(max_length=200, verbose_name='title')),
                ('slug', models.SlugField(verbose_name='Label')),
                ('header_title', models.CharField(max_length=1024, null=True, verbose_name='Header main title', blank=True)),
                ('sub_title', models.CharField(max_length=1024, null=True, verbose_name='Sub Title', blank=True)),
                ('image', models.ImageField(upload_to=lemoulin_pages.models.get_pages_image_path, null=True, verbose_name='Image', blank=True)),
                ('text', models.TextField(verbose_name='Content', blank=True)),
                ('template_name', models.CharField(max_length=70, help_text="Example: 'static/default.html'", null=True, verbose_name='template name', blank=True)),
            ],
            options={
                'ordering': ('title',),
                'verbose_name_plural': 'Pages',
                'verbose_name': 'Page',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PageMedia',
            fields=[
                ('media_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='lemoulin_gallery.Media', auto_created=True, serialize=False)),
                ('page', models.ForeignKey(related_name='media', to='lemoulin_pages.Page')),
            ],
            options={
                'verbose_name': 'Page Media',
                'verbose_name_plural': 'Page Media',
                'ordering': ['position', '-created'],
            },
            bases=('lemoulin_gallery.media',),
        ),
        migrations.CreateModel(
            name='PageSubSection',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, null=True, verbose_name='Title', blank=True)),
                ('text', models.TextField(verbose_name='Content', blank=True)),
                ('image', models.ImageField(upload_to=lemoulin_pages.models.get_pages_image_path, null=True, verbose_name='Image', blank=True)),
                ('credit', models.CharField(max_length=200, null=True, verbose_name='Image Credit', blank=True)),
                ('order', models.PositiveSmallIntegerField(null=True, verbose_name='Display order', blank=True)),
                ('page', models.ForeignKey(related_name='subsections', to='lemoulin_pages.Page')),
            ],
            options={
                'verbose_name': 'Page Sub Section',
                'verbose_name_plural': 'Pages Sub Sections',
                'ordering': ['order'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StaticText',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('language_code', models.CharField(max_length=10, choices=[('fr', 'Français')], verbose_name='Language', default='fr-ca')),
                ('title', models.CharField(max_length=200, null=True, verbose_name='Title', blank=True)),
                ('subtitle', models.CharField(max_length=200, null=True, verbose_name='Sub-Title', blank=True)),
                ('slug', models.SlugField(verbose_name='Label')),
                ('text', models.TextField(max_length=1024, null=True, verbose_name='Text', blank=True)),
            ],
            options={
                'ordering': ['slug'],
                'verbose_name_plural': 'Static Texts',
                'verbose_name': 'Static Text',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='statictext',
            unique_together=set([('language_code', 'slug')]),
        ),
        migrations.AlterUniqueTogether(
            name='page',
            unique_together=set([('language_code', 'slug')]),
        ),
    ]
