# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lemoulin_pages', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='statictext',
            name='text',
            field=models.TextField(verbose_name='Text', blank=True, null=True),
        ),
    ]
