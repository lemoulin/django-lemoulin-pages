from __future__ import unicode_literals

from django.contrib import admin
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.forms import ModelForm

if "suit_redactor" in settings.INSTALLED_APPS:
	from suit_redactor.widgets import RedactorWidget
elif "suit_ckeditor" in settings.INSTALLED_APPS:
	from suit_ckeditor.widgets import CKEditorWidget

from lemoulin_gallery.admin import MediaAdminInline, MediaForm

from . import models

DEFAULT_PAGE_MEDIA_TYPES = ["image", "video_url" ]

media_types = getattr(settings, "LE_MOULIN_PAGE_MEDIA_TYPES", DEFAULT_PAGE_MEDIA_TYPES)

class PageMediaInline(MediaAdminInline):
	form = MediaForm
	model = models.PageMedia
	extra = 1
	fieldsets = (
		(
			None,
			{
				'fields': ('title', 'description', 'credit', "position",)
			}
		),
	)

	if "image" in media_types:
		fieldsets = fieldsets + (
			(
				_("Image"),
				{
					'classes': ('collapse',),
					'fields': ('image',)
				}
			),
		)

	if "image_url" in media_types:
		fieldsets = fieldsets + (
			(
				_("Image URL"),
				{
					'classes': ('collapse closed',),
					'fields': ('image_url', "image_source", "image_embed_html", "image_thumbnail_url", "image_author_url", )
				}
			),
		)

	if "video_url" in media_types:
		fieldsets = fieldsets + (
			(
				_("Video URL"),
				{
					'classes': ('collapse',),
					'fields': ('video_url', "video_source", "video_width", "video_height", "video_thumbnail_url", "video_duration", "video_id",)
				}
			),
		)

	if "video_upload" in media_types:
		fieldsets = fieldsets + (
			(
				_("Uploaded Video"),
				{
					'classes': ('collapse',),
					'fields': ('video', 'video_thumbnail', )
				}
			),
		)

	if "file" in media_types:
		fieldsets = fieldsets + (
			(
				_("File"),
				{
					'classes': ('collapse',),
					'fields': ('file', 'file_thumbnail',)
				}
			),
		)


class PageSubSectionForm(ModelForm):
	class Meta:
		if "suit_redactor" in settings.INSTALLED_APPS:
			widgets = {
				'text': RedactorWidget(
					#editor_options={'lang': settings.LANGUAGE_CODE[:2], 'imageUpload':urlresolvers.reverse("lemoulin_gallery:ajax_media_upload"), 'imageGetJson':urlresolvers.reverse("lemoulin_gallery:ajax_media_list"),},
					editor_options={'lang': settings.LANGUAGE_CODE[:2], 'imageUpload':"/lemoulin_gallery/ajax/media/upload/", 'imageGetJson':"/lemoulin_gallery/ajax/media/list/",},
				),
			}
		elif "suit_ckeditor" in settings.INSTALLED_APPS:
			widgets = {
				'text': CKEditorWidget(),
			}
		elif settings.GRAPPELLI_URL:
			js = [
				settings.GRAPPELLI_URL+'tinymce/jscripts/tiny_mce/tiny_mce.js',
				settings.GRAPPELLI_URL+'tinymce_setup/tinymce_setup.js',
			]

class PageSubSection_Inline(admin.StackedInline):
	form = PageSubSectionForm
	model = models.PageSubSection
	extra = 1
	sortable_field_name = "order"
	can_delete = True

	class Media:
		if "suit_redactor" in settings.INSTALLED_APPS:
			widgets = {
				'text': RedactorWidget(
					#editor_options={'lang': settings.LANGUAGE_CODE[:2], 'imageUpload':urlresolvers.reverse("lemoulin_gallery:ajax_media_upload"), 'imageGetJson':urlresolvers.reverse("lemoulin_gallery:ajax_media_list"),},
					editor_options={'lang': settings.LANGUAGE_CODE[:2], 'imageUpload':"/lemoulin_gallery/ajax/media/upload/", 'imageGetJson':"/lemoulin_gallery/ajax/media/list/",},
				),
			}
		elif "suit_ckeditor" in settings.INSTALLED_APPS:
			widgets = {
				'text': CKEditorWidget(),
			}
		elif settings.GRAPPELLI_URL:
			js = [
				settings.GRAPPELLI_URL+'tinymce/jscripts/tiny_mce/tiny_mce.js',
				settings.GRAPPELLI_URL+'tinymce_setup/tinymce_setup.js',
			]


class PageForm(ModelForm):
	class Meta:
		if "suit_redactor" in settings.INSTALLED_APPS:
			widgets = {
				'text': RedactorWidget(
					#editor_options={'lang': settings.LANGUAGE_CODE[:2], 'imageUpload':urlresolvers.reverse("lemoulin_gallery:ajax_media_upload"), 'imageGetJson':urlresolvers.reverse("lemoulin_gallery:ajax_media_list"),},
					editor_options={'lang': settings.LANGUAGE_CODE[:2], 'imageUpload':"/lemoulin_gallery/ajax/media/upload/", 'imageGetJson':"/lemoulin_gallery/ajax/media/list/",},
				),
			}
		elif "suit_ckeditor" in settings.INSTALLED_APPS:
			widgets = {
				'text': CKEditorWidget(),
			}
		elif settings.GRAPPELLI_URL:
			js = [
				settings.GRAPPELLI_URL+'tinymce/jscripts/tiny_mce/tiny_mce.js',
				settings.GRAPPELLI_URL+'tinymce_setup/tinymce_setup.js',
			]

class PageAdmin(admin.ModelAdmin):
	form = PageForm
	list_display = ("title", "slug", "language_code", )
	ordering = ["title", "language_code", ]
	prepopulated_fields = {"slug": ("title", ), }
	search_fields = ["title", "language_code", "text", ]
	list_filter = ("language_code", )
	inlines = [PageSubSection_Inline, PageMediaInline, ]

	class Media:
		if "suit_redactor" in settings.INSTALLED_APPS:
			widgets = {
				'text': RedactorWidget(
					#editor_options={'lang': settings.LANGUAGE_CODE[:2], 'imageUpload':urlresolvers.reverse("lemoulin_gallery:ajax_media_upload"), 'imageGetJson':urlresolvers.reverse("lemoulin_gallery:ajax_media_list"),},
					editor_options={'lang': settings.LANGUAGE_CODE[:2], 'imageUpload':"/lemoulin_gallery/ajax/media/upload/", 'imageGetJson':"/lemoulin_gallery/ajax/media/list/",},
				),
			}
		elif "suit_ckeditor" in settings.INSTALLED_APPS:
			widgets = {
				'text': CKEditorWidget(),
			}
		elif settings.GRAPPELLI_URL:
			js = [
				settings.GRAPPELLI_URL+'tinymce/jscripts/tiny_mce/tiny_mce.js',
				settings.GRAPPELLI_URL+'tinymce_setup/tinymce_setup.js',
			]

admin.site.register(models.Page, PageAdmin)


class StaticTextForm(ModelForm):
	class Meta:
		if "suit_redactor" in settings.INSTALLED_APPS:
			widgets = {
				'text': RedactorWidget(
					#editor_options={'lang': settings.LANGUAGE_CODE[:2], 'imageUpload':urlresolvers.reverse("lemoulin_gallery:ajax_media_upload"), 'imageGetJson':urlresolvers.reverse("lemoulin_gallery:ajax_media_list"),},
					editor_options={'lang': settings.LANGUAGE_CODE[:2], 'imageUpload':"/lemoulin_gallery/ajax/media/upload/", 'imageGetJson':"/lemoulin_gallery/ajax/media/list/",},
				),
			}
		elif "suit_ckeditor" in settings.INSTALLED_APPS:
			widgets = {
				'text': CKEditorWidget(),
			}
		elif settings.GRAPPELLI_URL:
			js = [
				settings.GRAPPELLI_URL+'tinymce/jscripts/tiny_mce/tiny_mce.js',
				settings.GRAPPELLI_URL+'tinymce_setup/tinymce_setup.js',
			]

class StaticTextAdmin(admin.ModelAdmin):
	form = StaticTextForm
	list_display = ("slug", "title", "language_code", )
	ordering = ["slug", "language_code", ]
	prepopulated_fields = {"slug": ("title", ), }
	list_filter = ("language_code", )
	save_as = True

	class Media:
		if "GRAPPELLI_URL" in settings.INSTALLED_APPS:
			js = [
				settings.GRAPPELLI_URL+'tinymce/jscripts/tiny_mce/tiny_mce.js',
				settings.GRAPPELLI_URL+'tinymce_setup/tinymce_setup.js',
			]

admin.site.register(models.StaticText, StaticTextAdmin)
