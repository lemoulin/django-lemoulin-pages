from __future__ import unicode_literals

from django import template
from django.utils.translation import get_language

register = template.Library()

@register.inclusion_tag("pages/static_text.html", takes_context=True)
def get_static_text(context, slug, show_title=False, safe=True, striptags=False, show_edit=True, heading="h3", wrapper="span", wrapper_class="static-text-wrapper", ):
	from lemoulin_pages import models

	request = context["request"]
	language_code = get_language()[:2]

	try:
		static_text = models.StaticText.objects.get(language_code=language_code, slug=slug)
	except:
		static_text = None

	if not static_text:
		edit_link = True
	else:
		edit_link = False

	return {"context": context, "request": request, "slug": slug, "static_text": static_text, "heading": heading, "wrapper": wrapper, "wrapper_class": wrapper_class,  "show_title": show_title, "safe": safe, "striptags": striptags, "show_edit": show_edit, "edit_link": edit_link, "language_code": language_code, }


@register.simple_tag(takes_context=True)
def simple_static_text(context, slug, field="text"):
	from lemoulin_pages import models

	request = context["request"]
	language_code = request.LANGUAGE_CODE

	try:
		static_text = models.StaticText.objects.get(languagecode=language_code, slug=slug)
	except:
		static_text = None

	return getattr(static_text, field, "")
