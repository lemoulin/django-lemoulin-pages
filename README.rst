===============
Le Moulin Pages
===============

Static pages and texts for Django

.. contents::

Getting started
===============

Requirements
------------

Le Moulin Utils requires:

- Python 3.4
- Django >= 1.7
- django-lemoulin-utils
- django-lemoulin-gallery

Installation
------------

You can get Le Moulin Utils by using pip::

    $ pip install -e git+https://bitbucket.org/lemoulin/django-lemoulin-pages.git#egg=lemoulin_pages

To enable `lemoulin_pages` in your project you need to add it to `INSTALLED_APPS` in your projects
`settings.py` file::

    INSTALLED_APPS = (
        ...
        'lemoulin_pages',
        ...
    )

Usage
=====
